const express = require('express')
const http = require('http');
const app = express();
const server = http.createServer(app);
const io = require('socket.io').listen(server);

let cacheFake = [];

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/index.html");
})

app.get("/new", (req, res) => {
    const min = 1, max = 100;
    const password = (Math.random() * (max - min) + min).toFixed(0);
    const table = (Math.random() * (max - 90 - min) + min).toFixed(0);;

    cacheFake.push({ password, table });
    io.send(cacheFake);
    res.status(201).json({ message: "success" });
});

app.get("/clear", (req, res) => {
    cacheFake = [];
    io.send(cacheFake);
    res.status(200).json({ message: "success" });
});

app.get("/consume", (req, res) => {
    if (cacheFake && cacheFake.length > 0) {
        cacheFake.shift()
    }
    io.send(cacheFake);
    res.status(200).json({ message: "success" });

});

io.on("connection", (socket) => {
    io.send(cacheFake);
    console.log("new user ", socket.id);
})

io.on('connect', () => {
});

io.on('message', data => {
    console.log(data);
});

server.listen(4000);
